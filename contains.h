#ifndef CONTAINS_H
#define CONTAINS_H

#include <QString>
#include <QDate>

class Contains
{

public:
    explicit Contains();
    ~Contains();
    QString name = "";
    int num = 0;
    double money = 0;
    bool cd = false;
    QDate date = QDate::currentDate();
    enum Cat { JURNAL, GAZETA};
    enum Per {EVERYDAY, WEAK1, MONTH2, MONTH1, YEAR6, YEAR4, YEAR2, YEAR1};
    enum Format {INFO, MODA, SPORT, HEALTH, GARDEN, TRAVEL, TV};
    Cat cat = JURNAL;
    Per per = EVERYDAY;
    Format format = INFO;
};

#endif // CONTAINS_H
