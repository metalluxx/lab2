#include "mainwindow.h"
#include <QApplication>
#include "contains.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setAttribute(Qt::AA_Use96Dpi);
    MainWindow w;
    w.show();
    return a.exec();
}
