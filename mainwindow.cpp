#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "contains.h"//подключение класса для хранения данных
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   ui->setupUi(this);
    MainWindow::setWindowFlags(Qt::WindowMaximizeButtonHint);// скрыть кнопку развертывания окна
    uncheckAllPer(); // отчистка ui
    ui->dateEdit->setDate(QDate::currentDate());
    ui->dateEdit->setMaximumDate(QDate::currentDate());//изменение диапазона для виджета даты
    ui->dateEdit->setMinimumDate(QDate::currentDate().addYears(-200));
    tmpData = data1; //указываем на первую ячейку
    readData(tmpData);// и заполняем ui данными
    ui->statusBar->showMessage("Выбрана первая ячейка");
}

MainWindow::~MainWindow()
{
    delete ui;
    delete data1;//после завершения удаляем из дин. памяти объекты класса Contains
    delete data2;
}

// Слот:Изменение видимости виджетов QRatioButton при критерии:журнал
void MainWindow::on_cJur_pressed()
{
    uncheckAllPer();// чистим все qRB от прошлых данных
    ui->perEday->setEnabled(true);//делаем нужные qRB видимыми
    ui->perW1->setEnabled(true);
    ui->perM2->setEnabled(true);
    ui->perM1->setEnabled(true);

    ui->fHel->setEnabled(true);
    ui->fInfo->setEnabled(true);
    ui->fModa->setEnabled(true);
    ui->fSad->setEnabled(true);
    ui->fSport->setEnabled(true);
    ui->fTravel->setEnabled(true);

    ui->perEday->setCheckable(true);//даем возможноть выбирать варианты для нужных qRB
    ui->perW1->setCheckable(true);
    ui->perM2->setCheckable(true);
    ui->perM1->setCheckable(true);

    ui->fHel->setCheckable(true);
    ui->fInfo->setCheckable(true);
    ui->fModa->setCheckable(true);
    ui->fSad->setCheckable(true);
    ui->fSport->setCheckable(true);
    ui->fTravel->setCheckable(true);

    ui->moneycount->setMaximum(50000);//изменяем диапазон для выбора стоимости
    ui->moneycount->setMinimum(1500);

    ui->moneySlider->setMaximum(50000);
    ui->moneySlider->setMinimum(1500);

    if (ui->moneycount->text().toInt() <1500 and ui->moneycount->text().toInt()>50000)//проверка соответствия стоимости по критерию
    {   ui->moneycount->setValue(1500);
        ui->moneySlider->setValue(1500);
    }
}

// Слот:Изменение видимости виджетов QRatioButton при критерии:газета
void MainWindow::on_cGaz_pressed()
{
    uncheckAllPer();// чистим все qRB от прошлых данных
    ui->perM2->setEnabled(true);//делаем нужные qRB видимыми
    ui->perM1->setEnabled(true);
    ui->perY6->setEnabled(true);
    ui->perY4->setEnabled(true);
    ui->perY2->setEnabled(true);
    ui->perY1->setEnabled(true);

    ui->fHel->setEnabled(true);
    ui->fInfo->setEnabled(true);
    ui->fModa->setEnabled(true);
    ui->fSad->setEnabled(true);
    ui->fSport->setEnabled(true);
    ui->fTV->setEnabled(true);
    ui->fTravel->setEnabled(true);

    ui->perM2->setCheckable(true);//даем возможноть выбирать варианты для нужных qRB
    ui->perM1->setCheckable(true);
    ui->perY6->setCheckable(true);
    ui->perY4->setCheckable(true);
    ui->perY2->setCheckable(true);
    ui->perY1->setCheckable(true);

    ui->fHel->setCheckable(true);
    ui->fInfo->setCheckable(true);
    ui->fModa->setCheckable(true);
    ui->fSad->setCheckable(true);
    ui->fSport->setCheckable(true);
    ui->fTV->setCheckable(true);
    ui->fTravel->setCheckable(true);

    ui->moneycount->setMaximum(5000);//изменяем диапазон для выбора стоимости
    ui->moneycount->setMinimum(500);
    ui->moneySlider->setMaximum(5000);
    ui->moneySlider->setMinimum(500);

    if (ui->moneycount->text().toInt() <500 and ui->moneycount->text().toInt()>5000)//проверка соответствия стоимости
    {   ui->moneycount->setValue(500);
         ui->moneySlider->setValue(500);
    }
}

// Отключение виджетов RatioButton, зависимые от выбора критерия
void MainWindow::uncheckAllPer() {
    ui->perEday->setEnabled(false);//выключаем qRB
    ui->perW1->setEnabled(false);
    ui->perM2->setEnabled(false);
    ui->perM1->setEnabled(false);
    ui->perY6->setEnabled(false);
    ui->perY4->setEnabled(false);
    ui->perY2->setEnabled(false);
    ui->perY1->setEnabled(false);

    ui->fHel->setEnabled(false);
    ui->fInfo->setEnabled(false);
    ui->fModa->setEnabled(false);
    ui->fSad->setEnabled(false);
    ui->fSport->setEnabled(false);
    ui->fTV->setEnabled(false);
    ui->fTravel->setEnabled(false);

    ui->perEday->setCheckable(false);//убираем возможность выбирать qRB и чистим выборы в них
    ui->perW1->setCheckable(false);
    ui->perM2->setCheckable(false);
    ui->perM1->setCheckable(false);
    ui->perY6->setCheckable(false);
    ui->perY4->setCheckable(false);
    ui->perY2->setCheckable(false);
    ui->perY1->setCheckable(false);

    ui->fHel->setCheckable(false);
    ui->fInfo->setCheckable(false);
    ui->fModa->setCheckable(false);
    ui->fSad->setCheckable(false);
    ui->fSport->setCheckable(false);
    ui->fTV->setCheckable(false);
    ui->fTravel->setCheckable(false);
}

// Запись данных интерфейса в объект класса
void MainWindow::writeData(Contains *tmp)
{
    tmp->cd = ui->checkBox->isChecked();
    tmp->money = ui->moneycount->value();
    tmp->num = ui->numberId->value();
    tmp->date =  ui->dateEdit->date();
    tmp->name = ui->nameId->text();
    if (ui->cJur->isChecked()) (tmp->cat=Contains::JURNAL); else (tmp->cat=Contains::GAZETA);

    if (ui->perEday->isChecked()) (tmp->per = Contains::EVERYDAY);
    if (ui->perW1->isChecked()) (tmp->per = Contains::WEAK1);
    if (ui->perM2->isChecked()) (tmp->per = Contains::MONTH2);
    if (ui->perM1->isChecked()) tmp->per = Contains::MONTH1;
    if (ui->perY6->isChecked()) tmp->per = Contains::YEAR6;
    if (ui->perY4->isChecked()) tmp->per = Contains::YEAR4;
    if (ui->perY2->isChecked()) tmp->per = Contains::YEAR2;
    if (ui->perY1->isChecked()) tmp->per = Contains::YEAR1;

    if (ui->fInfo->isChecked()) (tmp->format = Contains::INFO);
    if (ui->fModa->isChecked()) (tmp->format = Contains::MODA);
    if (ui->fSport->isChecked()) (tmp->format = Contains::SPORT);
    if (ui->fHel->isChecked()) (tmp->format = Contains::HEALTH);
    if (ui->fSad->isChecked()) (tmp->format = Contains::GARDEN);
    if (ui->fTravel->isChecked()) (tmp->format = Contains::TRAVEL);
    if (ui->fTV->isChecked()) (tmp->format = Contains::TV);
}

//Заполнение интерфейса данными из объекта класса
void MainWindow::readData(Contains *tmp)
{
    ui->checkBox->setChecked(tmp->cd);
    ui->moneycount->setValue(tmp->money);
    ui->numberId->setValue(tmp->num);
    ui->dateEdit->setDate(tmp->date);
    ui->nameId->setText(tmp->name);
    if (tmp->cat==Contains::JURNAL) {
        ui->cJur->setChecked(true);
        on_cJur_pressed();
    } else {
        ui->cGaz->setChecked(true);
        on_cGaz_pressed();}

    if (tmp->per == Contains::EVERYDAY) ui->perEday->setChecked(true);
    if (tmp->per == Contains::WEAK1) ui->perW1->setChecked(true);
    if (tmp->per  == Contains::MONTH2) ui->perM2->setChecked(true);
    if (tmp->per == Contains::MONTH1) ui->perM1->setChecked(true);
    if (tmp->per == Contains::YEAR6) ui->perY6->setChecked(true);
    if (tmp->per == Contains::YEAR4) ui->perY4->setChecked(true);
    if (tmp->per == Contains::YEAR2) ui->perY2->setChecked(true);
    if (tmp->per == Contains::YEAR1) ui->perY1->setChecked(true);

    if (tmp->format == Contains::INFO) ui->fInfo->setChecked(true);
    if (tmp->format == Contains::MODA) ui->fModa->setChecked(true);
    if (tmp->format == Contains::SPORT) ui->fSport->setChecked(true);
    if (tmp->format == Contains::HEALTH) ui->fHel->setChecked(true);
    if (tmp->format == Contains::GARDEN) ui->fSad->setChecked(true);
    if (tmp->format == Contains::TRAVEL) ui->fTravel->setChecked(true);
    if (tmp->format == Contains::TV) ui->fTV->setChecked(true);
}

//Слот:Переключение на первую ячейку(нажатине на действие в тулбаре и меню)
void MainWindow::on_Data1_triggered()
{
    if(ui->nameId->text().isEmpty()){
        QMessageBox::warning(this, "Внимание", "Введите название индекса");
    }
    else{
    if(tmpData != data1)//если адреса открытой и выбранной ячейки совпадают, тогда не будет выполнено сохранение
    {
    MainWindow::writeData(tmpData);//запись текущей ячейки
    tmpData =  data1; //передача адреса первой ячейки
    uncheckAllPer();//предварительная чистка интерфейса
    MainWindow::readData(tmpData);//заполнение ui данными из нужной ячейки
    ui->statusBar->showMessage("Выбрана первая ячейка");}
}
}

//Слот:Переключение на вторую ячейку(нажатине на действие в тулбаре и меню)
void MainWindow::on_Data2_triggered()
{
    if(ui->nameId->text().isEmpty()){
        QMessageBox::warning(this, "Внимание", "Введите название индекса");
    }
    else{
    if(tmpData != data2){
    MainWindow::writeData(tmpData);
    tmpData =  data2;
    uncheckAllPer();
    MainWindow::readData(tmpData);
    ui->statusBar->showMessage("Выбрана первая ячейка");}
}
}

//4 слота для взаимной работы слайдера+поля_ввода денег и номера индекса
void MainWindow::on_numberSlider_sliderMoved(int position)
{
    ui->numberId->setValue(position);
}
void MainWindow::on_moneySlider_sliderMoved(int position)
{
    ui->moneycount->setValue(position);
}
void MainWindow::on_numberId_valueChanged(int arg1)
{
    ui->numberSlider->setValue(arg1);
}
void MainWindow::on_moneycount_valueChanged(double arg1)
{
    ui->moneySlider->setValue(int(arg1));
}

