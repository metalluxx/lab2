#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "contains.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

private slots:
    void on_cJur_pressed();

    void on_cGaz_pressed();

    void writeData(Contains *tmp);

    void readData(Contains *tmp);

    void on_Data1_triggered();

    void on_Data2_triggered();


    void on_numberSlider_sliderMoved(int position);

    void on_moneySlider_sliderMoved(int position);

    void on_numberId_valueChanged(int arg1);

    void on_moneycount_valueChanged(double arg1);



private:
    Ui::MainWindow *ui;
    Contains *tmpData;
    Contains *data1 = new Contains();
    Contains *data2 = new Contains();
    void uncheckAllPer();
};

#endif // MAINWINDOW_H
